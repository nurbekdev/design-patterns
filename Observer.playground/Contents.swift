import Darwin

protocol Observable {
    func addObserver(observer: Observer)
    func removeObserver(observer: Observer)
    func notifyObservers()
}

protocol Observer {
    var id: String { get set }
    func updatePrice(value: Int?)
}

final class Baraholka: Observable {
    private var stores: [Observer] = []
    
    var price: Int? {
        didSet {
            notifyObservers()
        }
    }
    
    func addObserver(observer: Observer) {
        stores.append(observer)
    }
    
    func removeObserver(observer: Observer) {
        guard let index = stores.enumerated().first(where: {
            $0.element.id == observer.id
        })?.offset else { return }
        stores.remove(at: index)
    }
    
    func notifyObservers() {
        stores.forEach { $0.updatePrice(value: price) }
    }
}
final class Magnum: Observer {
    var id = "magnum"
    func updatePrice(value: Int?) {
        guard let value = value else {
            return
        }
        print("prices was updated to \(value) in magnum ")
    }
}
final class Toimart: Observer {
    var id = "magnum"
    func updatePrice(value: Int?) {
        guard let value = value else {
            return
        }
        print("prices was updated to \(value) in toimart ")
    }
}
final class NurbekStore: Observer {
    var id = "NurbekStore"
    func updatePrice(value: Int?) {
        guard let value = value else {
            return
        }
        print("prices was updated to \(value) in NurbekStore ")
    }
}

let baraholka = Baraholka()

let magnum = Magnum()
let toimart = Toimart()
let nurbekStore = NurbekStore()

baraholka.addObserver(observer: magnum)
baraholka.addObserver(observer: toimart)


baraholka.price = 15
baraholka.addObserver(observer: nurbekStore)
baraholka.price = 12
